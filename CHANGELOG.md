﻿# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Unreleased

## [0.0.2] - 2022-08-??
### Added
- ReadMe file to document the bot itself
- Connection String in the appsettings
- Hug Command
- Basic leveling as now been added

# Latest Release 

## [0.0.1] - 2022-08-02
### Added
- This CHANGELOG file to keep an record of all changes to the bot
- NuGet packages for Discord.Net
- Set up the bot so that it can load up without erroring
- Ping Command

[Unreleased](https://github.com/LunarDevelop/Sancus-Discord/compare/V0.0.1-alpha...master)
[0.0.1](https://github.com/LunarDevelop/Sancus-Discord/releases/tag/V0.0.1-alpha)