﻿using MongoDB.Bson;

namespace SancusDiscord.Levels;

public class LevelModel
{
    public BsonObjectId Id { get; set; } = ObjectId.GenerateNewId();
    public ulong GuildId { get; set; }
    public int Level { get; set; }
    public int Required_Points { get; set; }
}

public class UserModel
{
    public BsonObjectId Id { get; set; } = ObjectId.GenerateNewId();
    public ulong GuildId { get; set; }
    public ulong UserId { get; set; }
    public int Points { get; set; } = 0;
    public int Level { get; set; } = 0;
}
