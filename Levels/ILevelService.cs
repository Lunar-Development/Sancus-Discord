﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SancusDiscord.Levels
{
    public interface ILevelService
    {
        public void NewUser(ulong guildId, ulong userId);
        public void DeleteUser(ulong guildId, ulong userId);
        public void UserSendMsg(ulong guildId, ulong userId);
        public bool CheckIfLevelUp(ulong guildId, ulong userId);
        public UserModel GetUserDetails(ulong guildId, ulong userId);
    }
}
