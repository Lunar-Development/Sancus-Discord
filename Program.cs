﻿namespace SancusDiscord;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SancusDiscord.Levels;

public partial class Program
{
    private DiscordSocketClient? _client;

    private readonly CommandService _commands;
    private readonly IServiceProvider _services;

    private IConfiguration? config;
    private readonly ILevelService _levelService;

    string environment = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "Production";

    private Program()
    {
        _client = new DiscordSocketClient(new DiscordSocketConfig()
        {
            LogLevel = LogSeverity.Info,

            // Limited amout of message caches to keep
            MessageCacheSize = 50,

        });

        _commands = new CommandService(new CommandServiceConfig()
        {
            LogLevel = LogSeverity.Info,

            CaseSensitiveCommands = false,
        });

        _client.Log += Log;
        _commands.Log += Log;

        _client.Ready += ClientReadyAsync;

        _client.SlashCommandExecuted += HandlerSlashCmdsAsync;
        _client.MessageReceived += GuildMsgReceivedAsync;

        _services = ConfigureServices();
        _levelService = _services.GetRequiredService<ILevelService>();
    }

    public static Task Main(string[] args) => new Program().MainAsync();

    public async Task MainAsync()
    {
        config = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.{environment}.json")
                .AddEnvironmentVariables()
                .Build();

        string token = config["Discord:Token"];

        await _client.LoginAsync(TokenType.Bot, token);
        await _client.StartAsync();

        // Block this task until the program is closed.
        await Task.Delay(-1);

    }

    private Task Log(LogMessage msg)
    {
        switch (msg.Severity)
        {
            case LogSeverity.Critical:
            case LogSeverity.Error:
                Console.ForegroundColor = ConsoleColor.Red;
                break;
            case LogSeverity.Warning:
                Console.ForegroundColor = ConsoleColor.Yellow;
                break;
            case LogSeverity.Info:
                Console.ForegroundColor = ConsoleColor.White;
                break;
            case LogSeverity.Verbose:
            case LogSeverity.Debug:
                Console.ForegroundColor = ConsoleColor.DarkGray;
                break;
        }
        Console.WriteLine($"{DateTime.Now.ToString("d/M/yyyy HH:mm:ss")} [{msg.Severity}] {msg.Source}: {msg.Message} {msg.Exception}");
        Console.ResetColor();
        return Task.CompletedTask;
    }

    // For external services that Client requires
    private static IServiceProvider ConfigureServices()
    {
        var map = new ServiceCollection()
            .AddScoped<ILevelService, LevelService>();

        // When all your required services are in the collection, build the container.
        // Tip: There's an overload taking in a 'validateScopes' bool to make sure
        // you haven't made any mistakes in your dependency graph.
        return map.BuildServiceProvider();
    }

    private async Task ClientReadyAsync()
    {
        await RegisterSlashCmdsAsync();
    }

    private async Task<Task> GuildMsgReceivedAsync(SocketMessage msg)
    {
        if (!msg.Author.IsBot)
        {
            var guildId = (msg.Channel as SocketGuildChannel).Guild.Id;
            if (_levelService.CheckIfLevelUp(guildId, msg.Author.Id))
            {
                var userLv = _levelService.GetUserDetails(guildId, msg.Author.Id);
                await msg.Channel.SendMessageAsync($"{msg.Author.Mention}, well done on leveling up!! \n You are now level {userLv.Level} with {userLv.Points} points", messageReference: new MessageReference(messageId: msg.Id));
            }
        }

        return Task.CompletedTask;
    }
}