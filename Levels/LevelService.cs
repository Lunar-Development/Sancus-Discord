﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace SancusDiscord.Levels;

public class LevelService : ILevelService
{

    public string ConnectionString;
    private readonly MongoClient client;
    private readonly IMongoDatabase database;

    private readonly IMongoCollection<UserModel> users;
    private readonly IMongoCollection<LevelModel> levels;

    public LevelService()
    {
        IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "Production"}.json")
                .AddEnvironmentVariables()
                .Build();

        ConnectionString = config.GetConnectionString("DefaultConnection");
        client = new(ConnectionString);
        database = client.GetDatabase("SancusDiscord");

        users = database.GetCollection<UserModel>("Users");
        levels = database.GetCollection<LevelModel>("Levels");
    }

    public bool CheckIfLevelUp(ulong guildId, ulong userId)
    {
        UserModel user;
        if (users.EstimatedDocumentCount() == 0)
        {
            throw new InvalidOperationException();
        }
        else
        {
            var userSearch = users.Find(x => x.GuildId == guildId && x.UserId == userId);

            if (userSearch.CountDocuments() > 0) user = userSearch.First();
            else return false;
        }

        LevelModel level;
        if (levels.EstimatedDocumentCount() == 0)
        {
            return false;
        }
        else
        {
            var levelSearch = levels.Find(x => x.GuildId == guildId && x.Level == (user.Level + 1));
            Console.WriteLine(levelSearch.CountDocuments());
            if (levelSearch.CountDocuments() > 0) level = levelSearch.First();
            else return false;

        }
        if (user.Points >= level.Required_Points)
        {
            user.Level++;

            var updateDetails = Builders<UserModel>.Update
               .SetOnInsert("_id", user.Id)
               .Set("UserId", user.UserId)
               .Set("GuildId", user.GuildId)
               .Set("Points", user.Points)
               .Set("Level", user.Level);

            var updateOptions = new UpdateOptions()
            {
                IsUpsert = false
            };

            users.UpdateOne<UserModel>(x => x.Id == user.Id, updateDetails, updateOptions);

            return true;
        }
        else return false;
    }


    public void DeleteUser(ulong guildId, ulong userId)
    {
        throw new NotImplementedException();
    }

    public void NewUser(ulong guildId, ulong userId)
    {
        users.InsertOne(new UserModel()
        {
            UserId = userId,
            GuildId = guildId
        });
    }

    public void UserSendMsg(ulong guildId, ulong userId)
    {
        UserModel user;
        if (users.EstimatedDocumentCount() == 0)
        {
            user = new()
            {
                UserId = userId,
                GuildId = guildId,
            };
        }
        else
        {
            var userSearch = users.Find(x => x.GuildId == guildId && x.UserId == userId);

            if (userSearch.CountDocuments() > 0) user = userSearch.First();
            else user = new()
            {
                UserId = userId,
                GuildId = guildId,
            };
        }

        user.Points++;

        var updateDetails = Builders<UserModel>.Update
               .SetOnInsert("_id", user.Id)
               .Set("UserId", user.UserId)
               .Set("GuildId", user.GuildId)
               .Set("Points", user.Points)
               .Set("Level", user.Level);

        var updateOptions = new UpdateOptions()
        {
            IsUpsert = true
        };

        users.UpdateOne<UserModel>(x => x.Id == user.Id, updateDetails, updateOptions);
    }

    public UserModel GetUserDetails(ulong guildId, ulong userId)
    {
        if (users.EstimatedDocumentCount() == 0)
        {
            throw new InvalidOperationException();
        }
        else
        {
            var userSearch = users.Find(x => x.GuildId == guildId && x.UserId == userId);

            if (userSearch.CountDocuments() > 0) return userSearch.First();
            else return new();
        }
    }
}
