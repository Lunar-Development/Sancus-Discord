﻿using Discord;
using Discord.WebSocket;

namespace SancusDiscord;

public partial class Program
{
    List<SlashCommandBuilder> GlobalSlashCommands = new List<SlashCommandBuilder>()
    {
        new SlashCommandBuilder()
        {
            Name = "ping",
            Description = "See the current ping of the bot"
        },
        new SlashCommandBuilder()
        {
            Name = "hug",
            Description = "Give a hug to anotheer user",
            Options = new List<SlashCommandOptionBuilder>()
            {
                new SlashCommandOptionBuilder()
                {
                    Name = "user",
                    Description = "Choose a user to send a hug to",
                    Type = ApplicationCommandOptionType.User
                }
            }
        }
    };

    public async Task RegisterSlashCmdsAsync()
    {
        foreach (var command in GlobalSlashCommands)
        try
        {
            Console.ForegroundColor = ConsoleColor.Green;
            await _client.CreateGlobalApplicationCommandAsync(command.Build());
            Console.WriteLine($"{DateTime.Now.ToString("d/M/yyyy HH:mm:ss")} [Success] Slash Commands: {command.Name} command has been added to the scope");
            Console.ResetColor();
        }
        catch (Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{DateTime.Now.ToString("d/M/yyyy HH:mm:ss")} [ERROR] Slash Commands: {command.Name} → {ex}");
            Console.ResetColor();
        }
    }

    public async Task HandlerSlashCmdsAsync(SocketSlashCommand cmd)
    {
        switch (cmd.CommandName)
        {
            case "ping":
                EmbedBuilder embed = new()
                {
                    Footer = new EmbedFooterBuilder()
                    {
                        Text = "Lunar Development",
                        IconUrl = "https://cdn.lunar-dev.com/Lunar-Dev/LunarDev-Icon-Small.png",
                    },
                    Title = "Ping!!!",
                    Description = "Pong!!",
                    Fields = new List<EmbedFieldBuilder>()
                    {
                        {
                            new EmbedFieldBuilder()
                            {
                                Name = "Bot's latency",
                                Value = _client.Latency
                            }
                        }
                    },
                    Color = Color.Gold
                };
                await cmd.RespondAsync(embed: embed.Build());
                break;

            case "hug":
                SocketUser huggedUser = cmd.Data.Options.First().Value as SocketUser;
                EmbedBuilder hugEmbed = new()
                {
                    Footer = new EmbedFooterBuilder()
                    {
                        Text = "Lunar Development",
                        IconUrl = "https://cdn.lunar-dev.com/Lunar-Dev/LunarDev-Icon-Small.png",
                    },
                    Title = "Huggs",
                    Description = $"{cmd.User.Mention} has sent a hug to {huggedUser.Mention}",
                    Color = Color.Red
                };
                await cmd.RespondAsync(embed: hugEmbed.Build());
                break;
        }
    }
}
